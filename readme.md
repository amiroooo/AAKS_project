DONE:
* login DONE
* connect/disconnect DONE
* open semester/choose semester DONE
* view Tasks/view courses/view submissions/results DONE
* create and send task solution DONE
* parent view children tasks/courses/submissions DONE
* parent cant watch children on ban DONE
* define course DONE
* back button DONE



TODO:
* define new task by teacher.
* add statistical information(teahcer class,class teacher,class course+ custom).
* define class.
* assign student to class.
* block parent by secretary.
* send request assign/remove teacher to courseInclass + assign student to courseInclass.
* send request assign/remove student to courseInclass + assign teacher to courseInclass.
* view profile self (parent student teacher).
* evaluate submission.
* generic evaluation form about course.
* school manager send answer on request.
* school manager can view everything.
 